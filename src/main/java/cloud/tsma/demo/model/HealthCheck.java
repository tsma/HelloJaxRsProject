package cloud.tsma.demo.model;

/**
 * Created by TSMA.
 */
public class HealthCheck {
    private boolean healthy;

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }
}
