package cloud.tsma.demo.model;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by TSMA.
 */
public class Info {
    private LocalDateTime creationDate;
    private String text;

    public Info() {
        creationDate = LocalDateTime.now();
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
