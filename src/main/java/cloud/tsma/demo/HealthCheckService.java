package cloud.tsma.demo;

import cloud.tsma.demo.model.HealthCheck;
import com.google.gson.Gson;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by TSMA.
 */
@Path("/health")
public class HealthCheckService {

    private Gson gson = new Gson();

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String healthCheck() {


        return gson.toJson(healthCheckObject());
    }

    private HealthCheck healthCheckObject() {

        HealthCheck check = new HealthCheck();
        check.setHealthy(true);
        return check;
    }
}
